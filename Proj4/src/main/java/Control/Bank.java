package Control;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Models.Account;
import Models.Persoana;
import View.BankProc;

public class Bank implements BankProc, Serializable{
	
	//banca contine hashmap de <Client, ArrayList<Account>>
	
	public HashMap<Persoana,ArrayList<Account>> banca = new HashMap<Persoana,ArrayList<Account>>();
	public ArrayList<Persoana> pers = new ArrayList<Persoana>();
	ArrayList<Account> cont = new ArrayList<Account>();
	
	public void updateAcc(Account c)
	{
		/*if
		 *if
		 *if
		 *assert caz fals return error
		 */
	}
	
	public void afPers()
	{
		for(Map.Entry<Persoana, ArrayList<Account>> entry : banca.entrySet())
		{
			System.out.println(entry.getKey());
		}
	}
	
	public void addPers(Persoana p)
	{
		pers.add(p);
	}
	
	public Account getCont(Persoana p, int id)
	{
		
		ArrayList<Account> t = banca.get(p);
		
		for(int i = 0; i < t.size(); i++)
		{
			if(t.get(i).contId == id)
				{
				return t.get(i);
				}
			
		}
		return null;
	}
	
	public Persoana getPers(int id)
	{
		assert id == 0 : "eroare id trebuie sa fie diferit de 0";
		
		for(Map.Entry<Persoana, ArrayList<Account>> entry : banca.entrySet())
		{
			if(entry.getKey().idPers == id)
				return entry.getKey();
		}
		
		
			return null;
	}
	
	public void updatePers(Persoana p,Account c)
	{
		assert p == null : "persoana nu exista";
		assert c == null : "contul nu exista";
		
		ArrayList<Account> t = banca.get(p);
		
		for(int i = 0; i < t.size(); i++)
		{
			if(t.get(i).contId == c.contId)
				{
				System.out.println("contul exista deja");
				return;
				}
			else
			{
				t.add(c);
				banca.put(p, t);
				System.out.println("updated acc");
				return;
			}
		}
		
	}
	
	public boolean checkAcc(Persoana p)
	{
		if(banca.get(p) == null)
			return true;
		
		return false;
	}
	
	public ArrayList<Account> createList(Persoana p, Account c)
	{
		assert p == null : "persoana nu exista";
		assert c == null : "contul nu exista";
		
		if(banca.get(p) == null)
			banca.get(p).add(c);
		else
			banca.get(p).add(c);
		return banca.get(p);
	}
	
	public void addClientH(Persoana p, ArrayList<Account> a)
	{
		assert p == null : "persoana nu exista";
		
		if(banca.containsKey(p))
			System.out.println("cheia deja existenta");
		else {
		banca.put(p, a);
		}

	}
	
	public void addContH(Persoana p, ArrayList<Account> c)
	{
		assert p == null : "persoana nu exista";
		
		banca.put(p,c);
	}
	
	public void removeClientH(Persoana p)
	{
		banca.remove(p);
	}
	
	public void removeAccountH(Persoana p,Account c)
	{
		ArrayList<Account> t = new ArrayList<Account>();
		
		t = banca.get(p);
		for(int i = 0; i < t.size(); i++)
		{
			if(t.get(i).contId == c.contId)
				t.remove(i);
			else
				continue;
		}
		System.out.println("removed - bank");
		System.out.println(t.toString());
		
		banca.put(p, t);
		
		
	}

	public ArrayList<String> getH(Persoana p)
	{
		ArrayList<String> s = new ArrayList<String>();
		
		s.add(String.valueOf(p.idPers));
		
		ArrayList<Account> t = banca.get(p);
		//System.out.println(t.size());
		
		for(int i = 0; i < t.size(); i++)
		{
			Account a = t.get(i);
			s.add(a.toString());
		}
		
		//System.out.println(s.toString());
		//System.out.println("----------------------");
		for(Map.Entry<Persoana, ArrayList<Account>> entry : banca.entrySet())
		{
			System.out.println(entry.getKey() + "===" + entry.getValue());
		}
		return s;
	}
	
	public ArrayList<String> getAccs(Persoana p)
	{
		ArrayList<Account> ret = new ArrayList<Account>();
		ArrayList<String> temp = new ArrayList<String>();
		
		ret = banca.get(p);
		
		for(int i = 0; i < ret.size(); i++)
			temp.add(ret.get(i).toString());
		
		System.out.println(temp.size());
		
		return temp;	
	}
	
	public ArrayList<Account> getAccountsList(Persoana p)
	{
		
		ArrayList<Account> cont = banca.get(p);
		
		return cont;
		
	}
	

	
}
