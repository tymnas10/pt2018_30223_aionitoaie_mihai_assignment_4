package Control;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class SaveLoad {
	
	
	//public Bank l;
	
	public Bank readData(String fileName)
	{
		Bank p = new Bank();
		try {
			ObjectInputStream is = new ObjectInputStream(new FileInputStream(fileName));

		
			p = (Bank) is.readObject();
			is.close();
			
		} catch (FileNotFoundException e) {
			return null;
			//e.printStackTrace();
		} catch (IOException e) {
			return null;
			//e.printStackTrace();
		} catch (ClassNotFoundException e) {
			return null;
			//e.printStackTrace();
		}

		return p;
	}
	
	public void saveData(String fileName,Object p)
	{
		
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(fileName));
			os.writeObject(p);
			os.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
