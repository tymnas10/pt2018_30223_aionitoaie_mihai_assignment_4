package Models;

import java.io.Serializable;

public class Persoana implements Serializable {
	
	public int idPers;
	public int pass;
	String nume;
	String varsta;
	String telefon;
	
	public Persoana()
	{
		
	}
	
	public Persoana(int id)
	{
		idPers = id;
	}
	
	public Persoana(int id, int pas)
	{
		idPers = id;
		pass = pas;
	}

	public Persoana(int id, String a, String b, String c)
	{
		idPers = id;
		nume = a;
		varsta = b;
		telefon = c;
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idPers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persoana other = (Persoana) obj;
		if (idPers != other.idPers)
			return false;
		return true;
	}

	public String toString(){
        return "idPers: "+idPers + " Parola: " + pass;
    }
	
}
