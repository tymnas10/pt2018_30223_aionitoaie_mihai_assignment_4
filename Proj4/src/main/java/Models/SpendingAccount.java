package Models;

import java.io.Serializable;
import java.util.ArrayList;

import Observers.Observer;

public class SpendingAccount implements Observer,Serializable {
	
	public int sum = 0;
	String tip = "spending";
	
	//se pot depozita sume variabile de bani
	public void depositMoney(int amount)
	{
		sum += amount;
	}
	
	public void withdrawMoney(int amount)
	{
		
		sum -= amount;
	}

	//public void update(int id, int amount, String cont,String tip) {
	public void update(ArrayList<Object> t)
	{
		//amount = suma
		// cont = spending/saving
		//System.out.println("Suma de :" + amount + " a fost " + tip + " contul " + cont + " al clientului cu id-ul : " + id);
		System.out.println("Suma de " + Integer.parseInt(t.get(2).toString()) + "lei a fost " + t.get(4).toString() +" contul---(" + tip + ") cu IDCont : " + t.get(3).toString() + " al clientului cu IDPersoana : "
				+ Integer.parseInt(t.get(1).toString()));
	}


	//update pentru deposit/retragere
}
