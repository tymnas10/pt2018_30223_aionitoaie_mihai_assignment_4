package Models;

import java.io.Serializable;
import java.util.ArrayList;

import Observers.Observer;
import Observers.Subject;

public class Account implements Subject,Serializable {
	
	public int contId;
	public SavingAccount sA;
	public SpendingAccount spA;
	ArrayList<Observer> listaObs = new ArrayList<Observer>();
	boolean modification = false;
		
	public Account(int a)
	{
		contId = a;
		sA = new SavingAccount();
		spA = new SpendingAccount();
	}
	
	public String toString(){
        //return "idCont: "+contId;
		return ""+contId;
    }

	public void registerObserver(Observer observer) {
		listaObs.clear();
		listaObs.add(observer);
		
	}

	public void removeObserver(Observer observer) {
		listaObs.remove(observer);
	}

	//public void notifyObservers(int id, int amount,String cont,String tip)
	public void notifyObservers(ArrayList<Object> temp)
	{

		for(Observer obs : listaObs)
		{
			//obs.update(id,amount,cont,tip);
			obs.update(temp);
		}
	}
	
	//public void setModification(boolean value,int id, int amount, String cont,String tip)
	public void setModification(ArrayList<Object> temp)
	{
	/*	modification = value;
		if(value == true)
		notifyObservers(id,amount,cont,tip);
	*/
		modification = (Boolean) temp.get(0);
		if(modification == true)
			notifyObservers(temp);
	}
	
		
	}

