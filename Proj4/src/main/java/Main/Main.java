package Main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import Control.Bank;
import Control.SaveLoad;
import View.GUI;

public class Main {

	static Bank b = new Bank();
	static Bank l;
	static SaveLoad sl = new SaveLoad();
	static String fileName = "dateBank.bin";
	
	public static void main(String[] args) {

		if(sl.readData(fileName) == null)
		sl.saveData(fileName, b);
			

			new GUI(sl.readData(fileName));

	}

}
