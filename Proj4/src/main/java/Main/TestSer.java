package Main;

import java.io.Serializable;

public class TestSer implements Serializable {

	int num;
	int age;
	String string;
	
	public TestSer(int nr,int nrr,String s)
	{
		num = nr;
		age = nrr;
		string = s;
	}
	
	public String toString()
	{
		return num + " " + age + " " + string;
	}
}
