package Observers;

import java.util.ArrayList;

public interface Subject {
		
		public void registerObserver(Observer observer);
		public void removeObserver(Observer observer);
		//public void notifyObservers(int id, int amount, String cont,String tip);
		public void notifyObservers(ArrayList<Object> temp);
}
