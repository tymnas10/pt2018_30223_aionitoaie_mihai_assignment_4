package View;

import java.util.ArrayList;

import Models.*;

public interface BankProc {

	/* primeste un id si returneaza persoana respectiva
	 * Pre: id-ul sa nu fie 0
	 * Post: se returneaza un obiect de tip Persoana
	 */
	public Persoana getPers(int id);
	
	
	/*preia toate conturile unei Persoane p si il transforma intr-un array de String
	 * Pre : Persoana p sa nu fie egala cu null
	 * Post : un arrayList de string-uri cu toate conturile persoanei p
	 */
	public ArrayList<String> getAccs(Persoana p);


	/*preia toate conturile unei Persoane p si il transforma intr-un array de Account
	 * Pre : Persoana p sa nu fie egala cu null
	 * Post : un arrayList de Accounts cu toate conturile Persoanei p
	 */
	public ArrayList<Account> getAccountsList(Persoana p);

	

	/* adauga toate conturile primite ca parametri persoanei 'p'
	 * Pre : Persoana p sa nu fie egala cu null
	 * Post : Persoana p prezinta conturile continute in ArrayList-ul 'c'
	 */
	public void addContH(Persoana p, ArrayList<Account> c);
	

	/* introduce o persoana in HashMap cu 0 sau mai multe conturi
	 * Pre : Persoana p sa nu fie egala cu null
	 * Post : o noua valoare in HashMap
	 */
	public void addClientH(Persoana p, ArrayList<Account> a);
	

	/* adauga un cont nou la lista de conturi a persoanei p si o returneaza
	 * Pre : Persoana p sa nu fie egala cu null/ Contul c sa nu fie egal cu null
	 * Post : un arrayList de string-uri cu toate conturile persoanei p
	 */
	public ArrayList<Account> createList(Persoana p, Account c);
	

	/* Actualizeaza datele unei anumite persoane dupa o schimbare de parametri
	 * Pre : Persoana p sa nu fie egala cu null/ Contul c sa nu fie egal cu null
	 * Post : O persoana re-actualizata
	 */
	public void updatePers(Persoana p, Account c);
	
}
