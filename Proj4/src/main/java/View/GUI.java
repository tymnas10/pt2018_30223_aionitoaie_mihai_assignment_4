package View;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import Control.Bank;
import Control.SaveLoad;
import Models.Account;
import Models.Persoana;
import Observers.Observer;
import Observers.Subject;

public class GUI {
	
	long timp;
	int valoareGlobala;
	
	JComboBox box;
	
	SaveLoad sl = new SaveLoad();
	String fileName = "dateBank.bin";
	
	int accesLevel = 0;//1 manager //0 user
	ArrayList<Object> tmp = new ArrayList<Object>();
	Bank b = new Bank();
	
	String[] generalRow = {"",""};
	
	JFrame frame = new JFrame("Bank");
	JFrame frameL = new JFrame("Log-in");
	JFrame frameU = new JFrame("User");
	
	JTextField user = new JTextField();
	JTextField pass = new JTextField();
	
	JTable table = new JTable();
	DefaultTableModel model = new DefaultTableModel();

	JComboBox comboBox ;TableColumn accountsCol ;
	
	JButton add,delete;
	
	Persoana a = new Persoana();
	
	JComboBox cb = new JComboBox();
	
	public void createJTable()
	{

		String[] col = {"IdClient","Accounts","SumaSaving","SumaSpending","interest"};
		model.setColumnIdentifiers(col);//model.addRow(row.toArray());
		table.setModel(model);
		
		table.setBackground(Color.GRAY);
		table.setForeground(Color.black);
		table.setFont(new Font("",1,20));
		table.setRowHeight(20);
		table.getSelectionModel().clearSelection();
		
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10,10,500,300);
		
		populareTabela();
		
		frame.add(pane);
	}

	public void generateUserView()
	{

		System.out.println("id pers = " + a.idPers);
		
		cb.removeAllItems();
		ArrayList<Account> acs = new ArrayList<Account>();
		acs = b.banca.get(a);
		System.out.println("acs " + acs.toString());

		final JTable tableU = new JTable();
		final DefaultTableModel modelU = new DefaultTableModel();
		
		String[] col = {"Id","SumaSaving","SumaSpending","interest"};
		modelU.setColumnIdentifiers(col); //model.addRow
		tableU.setModel(modelU);	
		
		cb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				modelU.setRowCount(0);
				try {
				Account t = b.getCont(a, Integer.parseInt(cb.getSelectedItem().toString()));
				//id -- suma saving -- suma spending -- interest
				String[] s = {String.valueOf(t.contId),String.valueOf(t.sA.sum),String.valueOf(t.spA.sum),String.valueOf(t.sA.interest)};
				modelU.addRow(s);
				
				if(t.sA.sum == 0)//0 savings
					valoareGlobala = 1;
				else if(t.spA.sum == 0)
					valoareGlobala = 0;
				
				System.out.println("val globala = " + valoareGlobala);
					
				
				
				
				}catch(Exception g)
				{
				}
			}
		});
		
		
		for(int i = 0; i < acs.size(); i++)
		{
			cb.addItem(acs.get(i).toString());
		}
		cb.setBounds(800,50,150,50);


		//custom table
		tableU.setBackground(Color.GRAY);
		tableU.setForeground(Color.black);
		tableU.setFont(new Font("",1,20));
		tableU.setRowHeight(20);
		
		JScrollPane pane = new JScrollPane(tableU);
		pane.setBounds(0,0,500,300);
		frameU.setLayout(null);
		
		
		JButton logOut = new JButton("Log Out");
		logOut.setBounds(10,700,100,30);
		
		logOut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				generateLogInFrame();
				frameU.dispose();
			}
		});
		
		
		//deposit//withdraw money
		JButton depositM, withdrawM;
		depositM = new JButton("deposit"); withdrawM = new JButton("withdraw");
		final JComboBox selectCont = new JComboBox();//se selecteaza contul in care se
		
		if(valoareGlobala == 0)
		selectCont.addItem("Savings Account");
		else if(valoareGlobala == 1)
		selectCont.addItem("Spending Account");
		
		depositM.setBounds(800,150,100,30);
		withdrawM.setBounds(800,200,100,30);
		
		depositM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				final JFrame fr = new JFrame();
				fr.setSize(300,300);
				fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				fr.setLayout(null);
				
				selectCont.setBounds(150,150,120,30);
				
				final JTextField sum = new JTextField();
				JLabel sumL = new JLabel("Suma");
				JButton sumB = new JButton("ADD");
				
				sumB.setBounds(130,200,100,30);
				sumL.setBounds(30,30,100,30);
				sum.setBounds(30,70,100,30);
				
				sumB.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						int nr = Integer.parseInt(modelU.getValueAt(0, 0).toString());
						int suma = Integer.parseInt(sum.getText());
						
						if(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - timp < 10)
							System.out.println("too early---wait time = " + (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - timp));
						
						else {
							timp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
						
						//System.out.println(selectCont.getSelectedIndex());
						Account temp = b.getCont(a, nr);
						
						if(selectCont.getSelectedIndex() == 0)
							//savings
						{
							temp.registerObserver(temp.sA);
							
							if(suma >= 500)
							{
								tmp.clear();
								temp.sA.sum += suma;
								
								tmp.add(true);tmp.add(a.idPers);tmp.add(suma);tmp.add(String.valueOf(temp.contId));tmp.add("adaugata in");
								temp.setModification(tmp);
								
								modelU.setRowCount(0);
								Account t = b.getCont(a, Integer.parseInt(cb.getSelectedItem().toString()));
								//id -- suma saving -- suma spending -- interest
								String[] s = {String.valueOf(t.contId),String.valueOf(t.sA.sum),String.valueOf(t.spA.sum),String.valueOf(t.sA.interest)};
								modelU.addRow(s);
								
								temp.removeObserver(temp.sA);
								
							}
							else
								System.out.println("suma minima pentru depunere = 500");
							
						}
						
						else
							//spending
						{
							temp.registerObserver(temp.spA);
							
							tmp.clear();
							tmp.add(true);tmp.add(a.idPers);tmp.add(suma);tmp.add(String.valueOf(temp.contId));tmp.add("adaugata in");
							temp.spA.sum += suma;
							temp.setModification(tmp);
							
							modelU.setRowCount(0);
							Account t = b.getCont(a, Integer.parseInt(cb.getSelectedItem().toString()));
							//id -- suma saving -- suma spending -- interest
							String[] s = {String.valueOf(t.contId),String.valueOf(t.sA.sum),String.valueOf(t.spA.sum),String.valueOf(t.sA.interest)};
							modelU.addRow(s);
						}
						fr.dispose();}
					}
				});
				
				
				fr.add(selectCont);
				fr.add(sum);fr.add(sumL);fr.add(sumB);
				fr.setVisible(true);
				
			}
		});
		
		withdrawM.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				final JFrame fr = new JFrame();
				fr.setSize(300,300);
				fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				fr.setLayout(null);
				
				selectCont.setBounds(150,150,120,30);
				
				final JTextField sum = new JTextField();
				JLabel sumL = new JLabel("Suma");
				JButton sumB = new JButton("Withdraw");
				
				sumB.setBounds(130,200,100,30);
				sumL.setBounds(30,30,100,30);
				sum.setBounds(30,70,100,30);
				
				sumB.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e)
					{
						int nr = Integer.parseInt(modelU.getValueAt(0, 0).toString());
						int suma = Integer.parseInt(sum.getText());
						
						System.out.println(selectCont.getSelectedIndex());
						Account temp = b.getCont(a, nr);
						
						if(TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - timp < 10)
							System.out.println("too early---wait time = " + (TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - timp));
						
						else {
							timp = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
						
						if(selectCont.getSelectedIndex() == 0)
							//savings
						{
							temp.registerObserver(temp.sA);
							
							if(suma <= temp.sA.sum)
							{
							tmp.clear();
							temp.sA.sum -= suma;
							tmp.add(true);tmp.add(a.idPers);tmp.add(suma);tmp.add(String.valueOf(temp.contId));tmp.add("extrasa din");
							//temp.setModification(true, a.idPers, suma, String.valueOf(temp.contId),"extrasa din");
							temp.setModification(tmp);
							
							modelU.setRowCount(0);
							Account t = b.getCont(a, Integer.parseInt(cb.getSelectedItem().toString()));
							//id -- suma saving -- suma spending -- interest
							String[] s = {String.valueOf(t.contId),String.valueOf(t.sA.sum),String.valueOf(t.spA.sum),String.valueOf(t.sA.interest)};
							modelU.addRow(s);
							
							temp.removeObserver(temp.sA);
							}
							else
								System.out.println("not enough money in acc");
							
						}
					
						else
							//spending
						{
							temp.registerObserver(temp.spA);
							
							if(suma <= temp.spA.sum)
							{
							tmp.clear();
							tmp.add(true);tmp.add(a.idPers);tmp.add(suma);tmp.add(String.valueOf(temp.contId));tmp.add("extrasa din");
							temp.spA.sum -= suma;
							//temp.setModification(true, a.idPers, suma, String.valueOf(temp.contId), "extrasa din");
							temp.setModification(tmp);
							
							modelU.setRowCount(0);
							Account t = b.getCont(a, Integer.parseInt(cb.getSelectedItem().toString()));
							//id -- suma saving -- suma spending -- interest
							String[] s = {String.valueOf(t.contId),String.valueOf(t.sA.sum),String.valueOf(t.spA.sum),String.valueOf(t.sA.interest)};
							modelU.addRow(s);
							}
							else
								System.out.println("not enough money in acc");
						}
						fr.dispose();}
					}
				});
				
				
				fr.add(selectCont);
				fr.add(sum);fr.add(sumL);fr.add(sumB);
				fr.setVisible(true);
				
			}
		});
		
		frameU.add(depositM);
		frameU.add(withdrawM);
		frameU.add(cb);
		frameU.add(logOut);
		frameU.setSize(1000,800);
		frameU.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameU.setVisible(true);
		
		frameU.add(pane);
		
	}
	
	public void generateManagerView()
	{

		final JTable tableU = new JTable();
		final JFrame frameM = new JFrame("Manager");
		
		frameM.addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e)
            {
                sl.saveData(fileName, b);//salveaza si daca nu se da log out
                e.getWindow().dispose();
            }
        });

		String[] col = {"IdClient","Accounts","SumaSaving","SumaSpending","interest"};
		model.setColumnIdentifiers(col);//model.addRow(row.toArray());
		table.setModel(model);
		
		table.setBackground(Color.GRAY);
		table.setForeground(Color.black);
		table.setFont(new Font("",1,20));
		table.setRowHeight(20);
		table.getSelectionModel().clearSelection();
		
		JScrollPane pane = new JScrollPane(table);
		pane.setBounds(10,10,500,300);
		
		populareTabela();
		
		frameM.add(pane);

		
		ArrayList<String> row = new ArrayList<String>();
		row.add("1");row.add("");row.add("222");row.add("111");row.add("5%");

		//set account to combo box
		accountsCol = table.getColumnModel().getColumn(1);
		
		
		frameM.setLayout(null);
		
		add = new JButton("Add Client"); add.setBounds(350,320,150,30);//add client
		delete = new JButton("Delete Client"); delete.setBounds(350,370,150,30);//delete client
		
		JButton addAc = new JButton("Add Cont"); addAc.setBounds(200,320,150,30);//add cont
		JButton deleteAc = new JButton("Delete Cont"); deleteAc.setBounds(200,370,150,30);//delete cont
		
		
		add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				generateAddFrame(1);
			}
		});
		
		delete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				generateDeleteFrame();
			}
		});
		
		addAc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				generateAddAccFrame();
			}
		});
		
		deleteAc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				generateDeleteAccFrame();
			}
		});
		
		JButton logOut1 = new JButton("Log Out");
		logOut1.setBounds(10,700,100,30);
		
		logOut1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				generateLogInFrame();
				frameM.dispose();
			}
		});
		
		frameM.add(logOut1);
		frameM.add(addAc); frameM.add(deleteAc);
		frameM.add(add); frameM.add(delete);
		frameM.setSize(1000,800);
		frameM.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameM.setVisible(true);
		
		
	}
	
	public void generateAddAccFrame()
	{
		final JFrame fr = new JFrame("Add Account");
		fr.setSize(300,300);
		fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fr.setLayout(null);
		
		final JTextField id = new JTextField(); JLabel idL = new JLabel("ID");
		final JTextField cont = new JTextField(); JLabel contL = new JLabel("Cont");
		JButton add2 = new JButton("Add");
		add2.setBounds(100,220,90,30);
		
		box = new JComboBox();
		box.addItem("Savings");
		box.addItem("Spending");
		
		box.setBounds(170,150,70,30);
		
		add2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				
				b.updatePers(b.getPers(Integer.parseInt(id.getText())), new Account(Integer.parseInt(cont.getText())));
				
				if(box.getSelectedIndex() == 0)//savings
				{
					Account temp = b.getCont(b.getPers(Integer.parseInt(id.getText())), Integer.parseInt(cont.getText()));
					temp.sA.sum = 100;
				}
				
				else
				{
					Account temp = b.getCont(b.getPers(Integer.parseInt(id.getText())), Integer.parseInt(cont.getText()));
					temp.spA.sum = 100;
				}
				populareTabela();
				
				fr.dispose();
			}
		});
		
		
		id.setBounds(50,50,100,30); cont.setBounds(50,150,100,30);
		idL.setBounds(50,20,100,30); contL.setBounds(50,120,100,30);
		
		fr.add(id);fr.add(cont);
		fr.add(box);
		fr.add(idL);fr.add(contL);
		fr.add(add2);
		fr.setVisible(true);
	}
	
	public void generateDeleteAccFrame()//todo
	{
		final JFrame fr = new JFrame("Delete Account");
		fr.setSize(300,300);
		fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fr.setLayout(null);
		
		final JTextField id = new JTextField(); JLabel idL = new JLabel("ID");
		final JTextField cont = new JTextField(); JLabel contL = new JLabel("Cont");
		JButton add2 = new JButton("Delete");
		add2.setBounds(100,220,90,30);
		
		add2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				b.removeAccountH(b.getPers(Integer.parseInt(id.getText())), new Account(Integer.parseInt(cont.getText())));
				populareTabela();
				System.out.println("deleted gui");
				fr.dispose();
			}
		});
		
		
		id.setBounds(50,50,100,30); cont.setBounds(50,150,100,30);
		idL.setBounds(50,20,100,30); contL.setBounds(50,120,100,30);
		
		fr.add(id);fr.add(cont);
		fr.add(idL);fr.add(contL);
		fr.add(add2);
		fr.setVisible(true);
	}
	
	public void generateDeleteFrame()
	{
		final JFrame fr = new JFrame("Delete client");
		fr.setSize(300,300);
		fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fr.setLayout(null);
		
		final JTextField id = new JTextField(); JLabel idL = new JLabel("ID");
		JButton add2 = new JButton("Delete");
		add2.setBounds(100,220,90,30);
		
		add2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				b.removeClientH(new Persoana(Integer.parseInt(id.getText())));
				populareTabela();
				
				fr.dispose();
			}
		});
		
		
		id.setBounds(50,50,100,30); //cont.setBounds(50,150,100,30);
		idL.setBounds(50,20,100,30);// contL.setBounds(50,120,100,30);
		
		fr.add(id);//fr.add(cont);
		fr.add(idL);//fr.add(contL);
		fr.add(add2);
		fr.setVisible(true);
	}

	public void generateAddFrame(int a)//a = acces level
	{
		final JFrame fr = new JFrame("Add Client");
		fr.setSize(300,300);
		fr.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fr.setLayout(null);
		
		if(a == 0)//user
		{
			
		}
		else//manager
		{
			
			box = new JComboBox();
			box.addItem("Savings");
			box.addItem("Spending");
			
			box.setBounds(170,150,70,30);
			
			final JTextField id = new JTextField(); JLabel idL = new JLabel("ID");
			final JTextField cont = new JTextField(); JLabel contL = new JLabel("Cont");
			JButton add2 = new JButton("Add");
			add2.setBounds(130,220,60,30);
			
			id.setBounds(50,50,100,30); cont.setBounds(50,150,100,30);
			idL.setBounds(50,20,100,30); contL.setBounds(50,120,100,30);
			
			add2.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e)
				{
					
					if(b.checkAcc(new Persoana(Integer.parseInt(id.getText()))))
						{
						System.out.println("Adding");
						ArrayList<Account> t = new ArrayList<Account>();
						t.add(new Account(Integer.parseInt(cont.getText())));
						b.addClientH(new Persoana(Integer.parseInt(id.getText()),Integer.parseInt(id.getText())), t);
						
						b.addPers(new Persoana(Integer.parseInt(id.getText())));
						
						model.addRow(b.getH(new Persoana(Integer.parseInt(id.getText()))).toArray());
						
						
						
						
						
					if(box.getSelectedIndex() == 0)//savings
					{
						Account temp = b.getCont(b.getPers(Integer.parseInt(id.getText())), Integer.parseInt(cont.getText()));
						temp.sA.sum = 100;
					}
					
					else
					{
						Account temp = b.getCont(b.getPers(Integer.parseInt(id.getText())), Integer.parseInt(cont.getText()));
						temp.spA.sum = 100;
					}
						}
					else
					{
						System.out.println("Existing");	
					}
					
					populareTabela();
					
					
					fr.setVisible(false);
				}
			});
			
			
			fr.add(box);
			fr.add(id);fr.add(cont);
			fr.add(idL);fr.add(contL);
			fr.add(add2);
			fr.setVisible(true);
		}
			
	}
	
	public void populareTabela()
	{
		model.setRowCount(0);
		
		
		for(Map.Entry<Persoana, ArrayList<Account>> entry : b.banca.entrySet())
		{
			
			Persoana a = entry.getKey();
			ArrayList<Account> b = entry.getValue();
			
			for(int i = 0; i < b.size(); i++) {
			ArrayList<String> t = new ArrayList<String>();
			t.add(String.valueOf(a.idPers));
			t.add(b.get(i).toString());
			t.add(String.valueOf(b.get(i).sA.sum));
			t.add(String.valueOf(b.get(i).spA.sum));
			t.add(String.valueOf(b.get(i).sA.interest));
			
			model.addRow(t.toArray());		
			}
		}
		
	
	}
	
	public void generateLogInFrame()
	{
		
		sl.saveData(fileName,b);
		System.out.println("done saving");
		
		frameL.setSize(550,350);
		frameL.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameL.setLayout(null);
		
		JButton bLog = new JButton("Log In");

		JLabel userL = new JLabel("User");
		JLabel passL = new JLabel("Pass");
		
		user.setBounds(200,100,150,30); user.setText("");
		pass.setBounds(200,150,150,30); pass.setText("");
		userL.setBounds(150,100,50,30);
		passL.setBounds(150,150,50,30);
		bLog.setBounds(230,200,70,30);
		
		frameL.add(bLog);
		frameL.add(user);frameL.add(userL);
		frameL.add(pass);frameL.add(passL);
		frameL.setVisible(true);
		
		bLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0)
			{
				frameU.dispose();
				switch(checkLogIn(user.getText(),pass.getText()))
				{
				case 0 : a = b.getPers(Integer.parseInt(user.getText()));generateUserView();frameL.dispose();break;
				case 1 : generateManagerView();frameL.dispose();break;
				case -1 : generateLogInFrame();frameL.dispose();break;
				}
				
			}
		});
		
	}
	
	public int checkLogIn(String s1, String s2)
	{
		int i = 0;
		b.afPers();
		
		if(s1.equals("") && s2.equals(""))
			return 1;
		
		
		try {
		Persoana p = b.getPers(Integer.parseInt(s1));

		System.out.println("persoana "+ p);
		if(p.pass == Integer.parseInt(s2))
			return 0;
		else
			System.out.println("Parola incorecta");
		
		}catch(Exception e)
		{
			System.out.println("Clientul nu exista");
		}
		
		return -1;
		
	}
	
	public GUI(Bank bank)
	{
		
		b = bank;
		
		long time = System.currentTimeMillis();
		long secunde = TimeUnit.MILLISECONDS.toSeconds(time);
		timp = secunde;

		frameL.setVisible(false);
		frame.setVisible(false);
		generateLogInFrame();
	
	}
	

}
